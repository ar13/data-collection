<?php
$key = "ef0841e1-62b5-43e1-870e-84815106c68f";
$url = "https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=%s";
$url = sprintf($url, $key);

$box = [
    'msgtype' => 'text',
    'text' => [
        'content' => 'api已更新'
    ]
];

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
curl_setopt($ch, CURLOPT_HTTPHEADER, array ('Content-Type: application/json;charset=utf-8'));
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($box));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$data = curl_exec($ch);
curl_close($ch);
