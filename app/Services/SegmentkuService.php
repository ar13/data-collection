<?php
/**
 * Created by PhpStorm.
 * User: surestdeng
 * Date: 2019/12/23
 * Time: 16:36:09
 */

namespace App\Services;

use App\Models\Article;
use App\Repositories\Handler\LearnkuHandler;
use Illuminate\Support\Arr;
use QL\QueryList;

class SegmentkuService
{
    public function run()
    {
        $urls = config('address.Segmentfault.list');
        $root = 'https://segmentfault.com/';

        foreach ($urls as $url => $tag) {
            $data = QueryList::Query($url, [
                'avatar' => ['.summary>.author img', 'src'],
                'title' => ['.summary>.title>a', 'text'],
                'url' => ['.summary>.title>a', 'href']
            ])->data;

            # 过滤
            $data = array_map(function ($item) use ($root){
                $item['title'] = compress_html($item['title']);
                $item['url'] = $root . $item['url'];
                return $item;
            }, $data);

            $learkn = new LearnkuHandler();
            $articles = $learkn->handle($data, 'Segmentfault');
            $this->insert($articles, $learkn, $tag);

        }
    }

    public function insert(array $articles, LearnkuHandler $learnkuHandler, $tag)
    {
        logEcho($articles, 'learnku.log');
        Article::query()->insert($articles);
        $learnkuHandler->save($articles);
        $learnkuHandler->saveTag($articles, $tag);
    }
}
