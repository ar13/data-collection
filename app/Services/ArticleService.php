<?php
/**
 * Created by PhpStorm.
 * User: surestdeng
 * Date: 2019/12/25
 * Time: 13:01:35
 */

namespace App\Services;


use App\Models\Article;
use App\Models\Platform;

class ArticleService
{
    protected $model;

    public function __construct()
    {
        $this->model = new Article();
    }

    public function getArtcles($page, $pageSize, $platform){
        $article = new Article();
        $platform_id = Platform::query()->where('name', $platform)->value('id');
        $total = Article::query()
            ->where('platform_id', $platform_id)
            ->where('state', 0)
            ->count();
        $list = $article->newQuery()
            ->where('platform_id', $platform_id)
            ->where('state', 0)
            ->orderByDesc('weight')
            ->forPage($page, $pageSize)
            ->get()->toArray();
        $data = compact('page', 'pageSize', 'list', 'total');

        return $data;
    }

    /**
     * 获取文章详情
     * User: surest
     * Date: 2020/1/9
     * @param string $id 文章ID
     * @return array
     */
    public function getDetail(string $id) : array
    {
        $article = $this->model->newQuery()->find($id);
        $article = $article ? $article->toArray() : [];

        return  $article;
    }
}
