<?php
/**
 * Created by PhpStorm.
 * User: surestdeng
 * Date: 2020/3/15
 * Time: 13:29:51
 */

namespace App\Services;


use Monolog\Formatter\LineFormatter;
use Monolog\Formatter\LogstashFormatter;
use Monolog\Handler\FirePHPHandler;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\SyslogHandler;
use Monolog\Logger;

class TestService
{
    public function monolog()
    {
        $logger = new Logger('data-collection');
//        $logger->pushHandler(new StreamHandler('./data/test.log', Logger::WARNING));
//        $logger->pushHandler(new SyslogHandler('./data/test.log'));
//        $log->pushHandler(new FirePHPHandler());
//
//        $log->warning('foo');
//        $logger->error('foo');
//        $log->addRecord(Logger::WARNING, 'add user', ['username' => 'username']);
//
//        // the default format is "Y-m-d H:i:s"
//        $dateFormat = "Y n j, g:i a";
//        $output = "%datetime% > %level_name% > %message% %context% %extra%\n";
//        $formatter = new LineFormatter($output, $dateFormat);
//
//        $stream = new StreamHandler('./data/my_app.log', Logger::DEBUG);
//        $stream->setFormatter($formatter);
//        // bind it to a logger object
//        $securityLogger = new Logger('security');
//        $securityLogger->pushHandler($stream);
//        $securityLogger->error('info', ['cc'=>'bb']);

//        $logger->pushProcessor(function ($record) {
//            $record['extra']['dummy'] = 'Hello world!';
//
//            return $record;
//        });


        $logger = new Logger('data-collection');
        $stream = new RotatingFileHandler('./data/test.log');
        $stream->setFormatter(new LogstashFormatter('data-collection'));
        $logger->pushHandler($stream);
        $logger->info('success', ['a' => 'a']);
    }
}
