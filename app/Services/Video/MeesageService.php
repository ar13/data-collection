<?php
/**
 * User: surestdeng
 * Date: 2020/4/13
 * Time: 10:44:13
 */

namespace App\Services\Video;


use App\Models\Video\MessagesModel;
use App\Models\Video\MessagesUserModel;
use App\Models\Video\ReplyModel;
use App\Models\Video\UserModel;
use Illuminate\Support\Arr;

class MeesageService
{
    /**
     * User: surest
     * Date: 2020/4/13
     */
    public function getMessage($reaed = 0, $page=5, $pageSize=10)
    {
        $user = getUserInfo(['*']);
        $messages_users = MessagesUserModel::query()
            ->where('user_id', $user['id'])
            ->where('readed', $reaed);

        $total = $messages_users->count();
        $messages_users = $messages_users->with('messages')
        ->forPage($page, $pageSize)->get(['id', 'message_id', 'readed'])->toArray();

        $messages_users = array_map(function ($item) {
            $messages = Arr::get($item, 'messages');
            $item = array_merge($item, $messages);
            if($messages) unset($item['messages']) ;
            return $item;
        },$messages_users);

        return [$total, $messages_users];
    }

    /**
     * 发起留言
     * User: surest
     * Date: 2020/4/13
     * @param $content
     * @param $link
     */
    public function reply($reply, $link)
    {
        $user = getUserInfo(['id']);
        $user_id = $user['id'];
        ReplyModel::query()->insert(compact('reply', 'link', 'user_id'));
    }
}
