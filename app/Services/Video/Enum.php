<?php
/**
 * User: surestdeng
 * Date: 2020/4/11
 * Time: 22:11:22
 */

namespace App\Services\Video;


class Enum
{
    const LOGIN_KEY = 'video:user:login:';
    const LOGIN_KEY_EXPIRE = 60*60*24;
}
