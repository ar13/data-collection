<?php
/**
 * User: surestdeng
 * Date: 2020/4/4
 * Time: 10:11:23
 */

namespace App\Http\Controllers\Video;


use App\Http\Controllers\BaseController;
use App\Services\Video\MeesageService;
use App\Services\Video\VideoService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class IndexController extends BaseController
{
    public function parse(Request $request, VideoService $service)
    {
        if(!$url = $request->input('url')) {
            return $this->response(-1, [], '无效url');
        }

        if($data = $service->tryParse($url)) {
            return $this->response(0, $data);
        }
        return $this->response(-1, null, '解析失败');
    }

    /**
     * 获取我的消息
     * User: surest
     * Date: 2020/4/13
     */
    public function messages(Request $request, MeesageService $service)
    {
        $page = $request->input('page', 1);
        $pageSize = $request->input('pageSize', 10);
        $reaed = $request->input('readed', 0);
        list($total, $messages) = $service->getMessage($reaed, $page, $pageSize);

        return $this->response(0, [
            'list' => $messages,
            'page' => $page,
            'pageSize' => $pageSize,
            'total' => $total
        ]);
    }

    /**
     * 发起留言
     * User: surest
     * Date: 2020/4/13
     */
    public function reply(Request $request, MeesageService $service)
    {
        $valid = Validator::make($request->all(), [
            'reply' => 'required|max:255',
            'link' => 'required'
        ]);
        if($valid->fails()) {
            return $this->response(-1, null, $valid->errors()->first());
        }

        $reply = $request->input('reply');
        $link = $request->input('link');
        $service->reply($reply, $link);

        return $this->response(0, null);
    }

    public function dowload(Request $request)
    {
//        $url
    }
}
