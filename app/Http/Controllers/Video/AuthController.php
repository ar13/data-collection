<?php
/**
 * User: surestdeng
 * Date: 2020/4/10
 * Time: 17:26:08
 */

namespace App\Http\Controllers\Video;


use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Services\Video\LoginService;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class AuthController extends BaseController
{
    /**
     * 获取用户信息
     * User: surest
     * Date: 2020/4/10
     */
    public function getUserInfo(Request $request, LoginService $service)
    {
        $token = $request->header('token');
        $user = $service->getUserInfo($token);
        return $this->response(0, compact('user'));
    }

    /**
     * 登录
     * User: surest
     * Date: 2020/4/10
     */
    public function login(Request $request, LoginService $service)
    {
        $valid = Validator::make($request->all(), [
            'code' => 'string|required',
            'nickName' => 'string|required',
            'avatarUrl' => 'string|required',
        ]);
        $data = $valid->validated();
        $code = Arr::get($data, 'code');
        $name = Arr::get($data, 'nickName');
        $avatar = Arr::get($data, 'avatarUrl');
        list($isTrue, $content) = $service->wxLogin($code, $name, $avatar);
        if(!$isTrue) {
            return $this->response(-1, [], $content);
        }

        return $this->response(0, ['token' => $content], 'success');
    }

    /**
     * 注销
     * User: surest
     * Date: 2020/4/10
     */
    public function logout(Request $request, LoginService $service)
    {
        $token = $request->header('token');
        $service->setLogout($token);
        return $this->response(0, []);
    }
}
