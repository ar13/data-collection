<?php
/**
 * Created by PhpStorm.
 * User: surestdeng
 * Date: 2020/1/10
 * Time: 16:02:41
 */

namespace App\Http\Controllers;


class IndexController  extends BaseController
{
    public function pull()
    {
        $path = base_path('install.sh');
        exec("sh $path >> pull.log");
        echo env('APP_KEY');
    }

    public function index()
    {
        dump(env('APP_ENV'), env('APP_DEBUG'));
    }
}
