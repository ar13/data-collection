<?php
/**
 * Created by PhpStorm.
 * User: surestdeng
 * Date: 2019/12/24
 * Time: 17:38:16
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    public function response(int $code = 0, array $data = [], string $msg = 'success')
    {
        return [
            'code' => $code,
            'data' => $data,
            'msg' => $msg
        ];
    }
}
