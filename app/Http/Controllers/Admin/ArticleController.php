<?php
/**
 * Created by PhpStorm.
 * User: surestdeng
 * Date: 2020/1/8
 * Time: 17:20:53
 */

namespace App\Http\Controllers\Admin;


use App\Models\Article;
use App\Services\ArticleService;
use Illuminate\Http\Request;

class ArticleController extends BaseController
{
    /**
     * 列表
     * User: surest
     * Date: 2020/1/8
     */
    public function index(ArticleService $articleService, Request $request)
    {
        $page = $request->input('page', 0);
        $pageSize = $request->input('pageSize', 10);
        $platform = $request->input('platform');

        $data = $articleService->getArtcles($page, $pageSize, $platform);

        return $this->response(0, $data);
    }

    public function show($id, ArticleService $articleService)
    {
        $article = $articleService->getDetail($id);

        return $this->response(0, $article);
    }
}
