<?php
/**
 * Created by PhpStorm.
 * User: surestdeng
 * Date: 2019/12/24
 * Time: 17:37:59
 */

namespace App\Http\Controllers;


use App\Models\Article;
use App\Models\Platform;
use App\Services\ArticleService;
use Illuminate\Http\Request;

class ArticleController extends BaseController
{
    public function index(ArticleService $articleService, Request $request)
    {
        $page = $request->input('page', 0);
        $pageSize = $request->input('pageSize', 10);
        $platform = $request->input('platform');

        $data = $articleService->getArtcles($page, $pageSize, $platform);

        return $this->response(0, $data);
    }
}
