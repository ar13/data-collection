<?php
/**
 * User: surestdeng
 * Date: 2020/4/11
 * Time: 22:38:07
 */

namespace App\Http\Middleware;


use App\Services\Video\Enum;
use Illuminate\Http\Request;

class AuthMiddle
{
    public function handle(Request $request, \Closure $next)
    {
        return $next($request);
        $redis = getRedis();
        $token = $request->header('token');
        $key = Enum::LOGIN_KEY . $token;
        if($token && $redis->get($key)) {
            return $next($request);
        }
        return [
            'code' => -1,
            'msg' => '用户已退出'
        ];

    }
}
