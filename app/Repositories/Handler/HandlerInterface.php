<?php
/**
 * Created by PhpStorm.
 * User: surestdeng
 * Date: 2019/12/24
 * Time: 14:13:16
 */

namespace App\Repositories\Handler;


interface HandlerInterface
{
    public function handle(array $data, string $platform) :array ;

    public function filter(array $data) :array ;
}
