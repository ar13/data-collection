<?php
/**
 * User: surestdeng
 * Date: 2020/4/13
 * Time: 09:58:41
 */

namespace App\Repositories\Video;


use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class VideoParseService implements VideoParseInterface
{
    protected $url;
    public function request() :array
    {
        $param = [
            'appid' => config('video.videoparse.app_id'),
            'appsecret' => config('video.videoparse.appsecret'),
            'url' => $this->url,
        ];
        $api_url ='https://api-sv.videoparse.cn/api/video/normalParse?'.http_build_query($param);
        $info = file_get_contents($api_url);
        $info = json_decode($info, true);
        return $info;
    }

    public function getData($url) :array
    {
        $this->url = $url;
        $info = $this->request();
        $code = Arr::get($info, 'code');
        $body = Arr::get($info, 'body');

        $data = [
            'platform_key' => Arr::get($body, 'source', '-'),
            'source_url' => $this->url,
            'target_url' => Arr::get($body, 'video_url', '-'),
            'img_url' => Arr::get($body, 'cover_url', '-'),
            'json' => json_encode($body),
            'title' => Arr::get($body, 'title', '-'),
        ];

        error_log(json_encode($info) . PHP_EOL , 3, storage_path('logs/video.log'));
        return [(boolean)($code == 0), $data];
    }
}
