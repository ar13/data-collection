<?php
/**
 * User: surestdeng
 * Date: 2020/4/13
 * Time: 09:57:13
 */

namespace App\Repositories\Video;


Interface VideoParseInterface
{
    public function request();

    public function getData($url);
}
