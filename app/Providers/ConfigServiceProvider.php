<?php
/**
 * Created by PhpStorm.
 * User: surestdeng
 * Date: 2019/12/24
 * Time: 14:35:48
 */

namespace App\Providers;


use Carbon\Laravel\ServiceProvider;

class ConfigServiceProvider extends ServiceProvider
{
    public function register()
    {
        $app = $this->app;
        $app->configure('database');
        $app->configure('address');
        $app->configure('video');
        $app->configure('wechat');
    }
}
