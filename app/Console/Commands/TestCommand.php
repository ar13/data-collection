<?php
/**
 * User: surestdeng
 * Date: 2020/4/23
 * Time: 19:14:49
 */

namespace App\Console\Commands;

use App\Models\TOrder;
use Illuminate\Console\Command;
class TestCommand extends Command
{
    protected $signature = "order:test";

    public function handle()
    {
        TOrder::query()->increment('');
    }
}
