<?php
/**
 * User: surestdeng
 * Date: 2020/4/13
 * Time: 14:34:13
 */

namespace App\Console\Commands;

use Psy\Command\Command;

class VersionTypeCommand extends Command
{
    /**
     * 命令行的名称及签名。
     *
     * @var string
     */
    protected $signature = 'version_type:publish';

    /**
     * 命令行的描述
     *
     * @var string
     */
    protected $description = '版本发布';

    /**
     * 创建新的命令行实例。
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 执行命令行。
     *
     * @return mixed
     */
    public function handle()
    {
        $data = [
            [
                'version_type' => 5,
                'desc' => '5元20次'
            ],
            [
                'version_type' => 10,
                'desc' => '10元200'
            ]
        ];
    }
}
