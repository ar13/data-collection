<?php
/**
 * Created by PhpStorm.
 * User: surestdeng
 * Date: 2019/12/23
 * Time: 16:34:05
 */

namespace App\Console\Commands;


use App\Services\LearnkuService;
use App\Services\ZhihukuService;
use Illuminate\Console\Command;

class ZhihuDataCommand extends Command
{
    /**
     * 命令行的名称及签名。
     *
     * @var string
     */
    protected $signature = 'obtain:zhihu';

    /**
     * 命令行的描述
     *
     * @var string
     */
    protected $description = '爬一下知乎';

    /**
     * 创建新的命令行实例。
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 执行命令行。
     *
     * @return mixed
     */
    public function handle(ZhihukuService $zhihukuService)
    {
        # 爬取列表
        $zhihukuService->run();
        echo "list : success\n";
    }
}
