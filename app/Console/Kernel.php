<?php

namespace App\Console;

use App\Console\Commands\LearnkuDataCommand;
use App\Console\Commands\SegmentfaultCommand;
use App\Console\Commands\ZhihuDataCommand;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        LearnkuDataCommand::class,
        ZhihuDataCommand::class,
        SegmentfaultCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //
    }
}
