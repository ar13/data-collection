<?php
/**
 * Created by PhpStorm.
 * User: surestdeng
 * Date: 2019/12/23
 * Time: 17:23:35
 */

use App\Models\Video\UserModel;
use Predis\Client;
/**
 * 压缩html : 清除换行符,清除制表符,去掉注释标记
 * @param $string
 * @return $string
 * */
function compress_html($string) {
    $string = str_replace("\r\n", '', $string); //清除换行符
    $string = str_replace("\n", '', $string); //清除换行符
    $string = str_replace("\t", '', $string); //清除制表符
    $pattern = array(
        "/> *([^ ]*) *</", //去掉注释标记
        "/[\s]+/",
        "/<!--[^!]*-->/",
        "/\" /",
        "/ \"/",
        "'/\*[^*]*\*/'"
    );
    $replace = array(
        ">\\1<",
        " ",
        "",
        "\"",
        "\"",
        ""
    );
    return preg_replace($pattern, $replace, $string);
}

function logEcho($data, $file_name) {
    $data = is_array($data) ? json_encode($data) : $data;
    error_log($data . "\n",'3', storage_path("logs/") . $file_name);
}

function getRedis($key = 'default') {
    $client = new Client(config('database.redis' . $key));
    $client->select(2);
    return $client;
}

function getUserInfo($columns = ['*']) {
    if(env('APP_ENV') == 'local') {
        $id = 'ofu635YjRL2LbvS-wKq_7pu4l_hk';
    }else{
        $token = request()->header('token');
        $id = getRedis()->get($token);
    }

    return UserModel::getUser($id, $columns);
}
