<?php
/**
 * User: surestdeng
 * Date: 2020/4/23
 * Time: 19:21:54
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class TOrder extends Model
{
    protected $table = 't_order';
    protected $guarded = [];
}
