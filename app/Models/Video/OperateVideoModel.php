<?php
/**
 * User: surestdeng
 * Date: 2020/4/10
 * Time: 17:28:42
 */

namespace App\Models\Video;


use App\Models\BaseModel;

class OperateVideoModel extends BaseModel
{
    protected $table = 'v_operate_video';
    protected $guarded = [];
}
