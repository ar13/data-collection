<?php
/**
 * User: surestdeng
 * Date: 2020/4/10
 * Time: 17:28:42
 */

namespace App\Models\Video;


use App\Models\BaseModel;

class MessagesModel extends BaseModel
{
    protected $table = 'v_message';
    protected $guarded = [];
}
