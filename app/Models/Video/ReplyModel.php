<?php
/**
 * User: surestdeng
 * Date: 2020/4/10
 * Time: 17:28:42
 */

namespace App\Models\Video;


use App\Models\BaseModel;

class ReplyModel extends BaseModel
{
    protected $table = 'v_reply';
    protected $guarded = [];
}
