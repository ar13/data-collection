<?php
/**
 * User: surestdeng
 * Date: 2020/4/10
 * Time: 17:28:42
 */

namespace App\Models\Video;


use App\Models\BaseModel;

class UserModel extends BaseModel
{
    protected $table = 'v_users';
    protected $guarded = [];

    public static function getUser($id, $columns = [])
    {
        $user = self::query()->where('open_id', $id)->first($columns);
        return $user ? $user->toArray() : [];
    }
}
