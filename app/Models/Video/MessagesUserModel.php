<?php
/**
 * User: surestdeng
 * Date: 2020/4/10
 * Time: 17:28:42
 */

namespace App\Models\Video;


use App\Models\BaseModel;

class MessagesUserModel extends BaseModel
{
    protected $table = 'v_message_user_id';
    protected $guarded = [];

    public function messages()
    {
        return $this->hasOne(MessagesModel::class, 'id', 'message_id')->select(['message', 'id', 'level']);
    }
}
