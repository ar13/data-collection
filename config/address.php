<?php
/**
 * Created by PhpStorm.
 * User: surestdeng
 * Date: 2019/12/24
 * Time: 12:15:16
 */

return [
    "Learnku" => [
        'list' => [
            'https://learnku.com/laravel?filter=excellent&order=score' => [
                17, 18, 19
            ],
            'https://learnku.com/?filter=excellent' => [
                17, 19
            ]

        ]
    ],
    "知乎" => [
        'list' => [
            'https://www.zhihu.com/hot' => [
                1, 16
            ],
            'https://www.zhihu.com/hot?list=science' => [
                1, 16, 2
            ],
        ]
    ],
    "Segmentfault" => [
        'list' => [
            'https://segmentfault.com/blogs/hottest' => [
                15, 16
            ]
        ]
    ]
];
