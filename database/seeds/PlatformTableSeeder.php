<?php

use App\Models\Platform;
use Illuminate\Database\Seeder;

class PlatformTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = $this->getData();

        Platform::query()->insert($data);
    }

    public function getData()
    {
        $data = [
            [
                'name' => '知乎',
                'url' => 'https://www.zhihu.com/',
                'user_id' => 1
            ],
            [
                'name' => 'Learnku',
                'url' => 'https://learnku.com/',
                'user_id' => 1
            ],
            [
                'name' => 'Segmentfault',
                'url' => 'https://segmentfault.com/',
                'user_id' => 1
            ],
        ];

        return $data;
    }
}
