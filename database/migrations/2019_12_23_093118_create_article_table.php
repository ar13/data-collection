<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id')->comment('主键ID');
            $table->string('title')->comment('标题');
            $table->string('avatar')->default('')->comment('用户头像');
            $table->string('url')->comment('链接');
            $table->string('content')->default('')->comment('内容');
            $table->string('platform_id')->comment('平台ID');
            $table->string('comment_id')->default(0)->comment('评论ID');
            $table->string('tag_id')->default(0)->comment('tagID');
            $table->string('weight')->default(0)->comment('权重');
            $table->string('state')->default(0)->comment('是否隐藏 0显示 1隐藏');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('article', function (Blueprint $table) {
            //
        });
    }
}
