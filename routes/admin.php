<?php

/**
 * 后台管理路由文件
 */


$router->group(['prefix' => 'admin', 'namespace' => 'Admin'], function ($router) {
    $router->get('/article', 'ArticleController@index');
    $router->get('/article/{id}', 'ArticleController@show');
});
