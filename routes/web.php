<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', 'IndexController@index');

$router->group(['prefix' => 'api'], function ($router){
    $router->get('/articles', 'ArticleController@index');
    $router->post('/pull', 'IndexController@pull');

    $router->group(['namespace' => 'Video'], function ($router) {
        $router->post('/login', 'AuthController@login');
        $router->get('/parse', 'IndexController@parse');

        $router->group(['middleware' => 'video_auth'], function ($router) {
            $router->get('/user/me', 'AuthController@getUserInfo');
            $router->get('/user/logout', 'AuthController@logout');
            $router->get('/messages', 'IndexController@messages');
            $router->get('/reply', 'IndexController@reply');
        });
    });

});
